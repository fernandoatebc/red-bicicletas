var Bicicleta = require('../../models/bicicleta');

beforeEach(()=>{Bicicleta.allBicis =[];});

describe('Bicicleta.allBicis',()=>{
    it('comeinza vacio',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',()=>{
    it('Agregamos una',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[20.1738096,-98.0603297]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById',()=>{
    it('debe devolverla bici con id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[20.1738096,-98.0603297]);
        var b = new Bicicleta(2,'azul','urbana',[20.1779374,-98.0584309]);

        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
        


    });
});
