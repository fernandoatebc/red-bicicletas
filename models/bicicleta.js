var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString=function (){
    return `id: ${id} | color: ${color}`
}

Bicicleta.allBicis = [];

Bicicleta.add = function (aBici){
    Bicicleta.allBicis.push(aBici);
}

//var a = new Bicicleta(1,'rojo','urbana',[20.1738096,-98.0603297]);
//var b = new Bicicleta(2,'azul','urbana',[20.1779374,-98.0584309]);
//var c = new Bicicleta(3,'morada','terraceria',[20.1774374,-98.0584309]);
//Bicicleta.add(a);
//Bicicleta.add(b);
//Bicicleta.add(c);


Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x=>x.id == aBiciId);
    if(aBici)
        return aBici
    else
        throw new Error(`No existe una bici con el id: ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

module.exports = Bicicleta;