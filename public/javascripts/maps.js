var mymap = L.map('main_map').setView([20.175217, -98.0528817], 15);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZmVyY2MxMDk3IiwiYSI6ImNrOWJxZGZ0dzI1emMzb2wyejNmMHNtaG8ifQ.qVu6miwSQbIYrfo21OmVKA'
}).addTo(mymap);



$.ajax({
    dataType:"json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion).addTo(mymap);
        })   
    }
})